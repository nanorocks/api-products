<?php

header('Content-type:application/json;charset=utf-8');

include_once("db/conn/db.php");
include_once("db/model/product.php");

$d = new Db();
$p = new Product($d->get_conn());

if($_SERVER['REQUEST_METHOD'] != 'DELETE')
{
    echo '{';
    echo '"Request": "Invalid reqest."';
    echo '}';  
    die();  
}

if(!isset($_REQUEST['id']))
{
    echo '{';
    echo '"Request": "No parameters given."';
    echo '}';  
    die(); 
}

$id=$_REQUEST['id'];
$p->id = $id;
echo $p->delete();
