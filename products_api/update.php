<?php

header('Content-type:application/json;charset=utf-8');

include_once("db/conn/db.php");
include_once("db/model/product.php");

$d = new Db();
$p = new Product($d->get_conn());

if($_SERVER['REQUEST_METHOD'] != 'POST')
{
    echo '{';
    echo '"Request": "Invalid reqest."';
    echo '}';  
    die();  
}

if(!isset($_REQUEST['id']) && !isset($_REQUEST['name']) && !isset($_REQUEST['country']) && !isset($_REQUEST['cost']))
{
    echo '{';
    echo '"Request": "No parameters given."';
    echo '}';  
    die(); 
}

$id = $_REQUEST['id'];
$name = $_REQUEST['name'];
$country = $_REQUEST['country'];
$cost = $_REQUEST['cost'];

$p->id = $id;
$p->name = $name;
$p->country = $country;
$p->cost = $cost;

echo $p->update();
