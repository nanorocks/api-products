<?php

header('Content-type:application/json;charset=utf-8');

include_once("db/conn/db.php");
include_once("db/model/product.php");

$d = new Db();
$p = new Product($d->get_conn());

if($_SERVER['REQUEST_METHOD'] != 'POST')
{
    echo '{';
    echo '"Request": "Invalid reqest."';
    echo '}';  
    die();  
}

if(!isset($_POST['name']) && !isset($_POST['country']) && !isset($_POST['cost']))
{
    echo '{';
    echo '"Request": "No parameters given."';
    echo '}';  
    die(); 
}

$name = $_POST['name'];
$country = $_POST['country'];
$cost = $_POST['cost'];

$p->name = $name;
$p->country = $country;
$p->cost = $cost;

echo $p->create();
