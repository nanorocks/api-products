<?php

header('Content-type:application/json;charset=utf-8');

include_once("db/conn/db.php");
include_once("db/model/product.php");

$d = new Db();
$p = new Product($d->get_conn());

if(isset($_GET['id']))
{

   $p->id = $_GET['id'];
   echo json_encode($p->read_one());

}elseif(isset($_GET['search']))
{
  
    $p->name = $_GET['search'];
    echo json_encode($p->search());

}elseif(isset($_GET['p']))
{
    $p->limit = 3;
    $p->offset = $_GET['p'];
    echo json_encode($p->paginate());
}
else{
   echo json_encode($p->read());
}
