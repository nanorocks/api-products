<?php

    class Product
    {
      public $id;
      public $name;
      public $country;
      public $cost;
      public $conn=null;
      public $limit;
      public $offset;

      private $table = "product";

      function __construct($conn)
      {
        $this->conn = $conn;
        $this->id = $this->name = $this->country = $this->cost = $this->limit = $this->offset = null;
      }

      function read()
      {
        $q = "SELECT * FROM " . $this->table . ";";
        $s = $this->conn->prepare($q);
        $s->execute();
        return $s->fetchAll();
      }

      function read_one()
      {
        $q = "SELECT * FROM " . $this->table . ' WHERE id=' . $this->id . ";";
        $s = $this->conn->prepare($q);
        $s->execute();
        return $s->fetchAll();
      }

      function search()
      {
        $q = "SELECT * FROM " . $this->table . " WHERE name LIKE '" . $this->name . "%';";
        $s = $this->conn->prepare($q);
        $s->execute();
        return $s->fetchAll();
      }

      function paginate()
      {
        $q = "SELECT * FROM " . $this->table . " LIMIT " . $this->limit . " OFFSET " . $this->offset . ";";
        $s = $this->conn->prepare($q);
        $s->execute();
        return $s->fetchAll();
      }

      function delete()
      {
        $success = 0;
        try{
          $q = "DELETE FROM " . $this->table . ' WHERE id=:id;';
          $s = $this->conn->prepare($q);
          $s->bindValue(':id', $this->id);
          $success = $s->execute();
        }
        catch(PDOException $e)
        {
          echo $e;
        }
        if($success)
        {
          return '{"Request":"Product deleted."}';
        }
        return '{"Request":"Product NOT deleted."}';
      } 

      function create()
      {
        $q = "INSERT INTO " . $this->table .  " (name, country, cost) VALUES " . "(':name', ':country', ':cost');";
        $s = $this->conn->prepare($q);
        $s->bindValue(':name', $this->name);
        $s->bindValue(':country', $this->country);
        $s->bindValue(':cost', $this->cost);
        $success = $s->execute();
    
        if($success)
        {
          return '{"Request":"Product created."}';
        }
        return '{"Request":"Product NOT created."}';
      }

      function update()
      {
        $q = "UPDATE " . $this->table . " SET name=:name, country=:country, cost=:cost WHERE id=:id;";
        $s = $this->conn->prepare($q);
  
        $s->bindValue(':id', $this->id);
        $s->bindValue(':name', $this->name);
        $s->bindValue(':country', $this->country);
        $s->bindValue(':cost', $this->cost);
        $success = $s->execute();
    
        if($success)
        {
          return '{"Request":"Product updated."}';
        }
        return '{"Request":"Product NOT updated."}';
  
      }
    
    }

   
