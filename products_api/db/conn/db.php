<?php

class Db{

    public $conn = null;
   /*
    * Private ctor so nobody else can instantiate it
    */
   public function __construct()
   {
     try {
         $this->conn = new PDO("sqlite:database.sqlite");
         // set the PDO error mode to exception
         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         #echo "Connected successfully";
         
         }
      catch(PDOException $e)
       {
       echo "Connection failed: " . $e->getMessage();
       }
   }
   public function get_conn(){
        return $this->conn;
   }


}
